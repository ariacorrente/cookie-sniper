.PHONY: clean

WEB-EXT="./node_modules/web-ext/bin/web-ext"
ROLLUP="./node_modules/rollup/dist/bin/rollup"

# Populate the folder with the extension ready
build: build/manifest.json \
	build/browser-polyfill.js \
	build/background.js \
	build/options.html \
	build/options.js \
	build/cookie-sniper.css \
	build/icons/cookie-sniper-19.png \
	build/icons/cookie-sniper-38.png \
	build/icons/cookie-sniper-48.png \
	build/icons/cookie-sniper-96.png \
	build/icons/delete.svg \
	build/icons/new.svg \
	build/icons/save.svg \
	build/README.md \
	build/LICENSE

# Build the compressed package for installing in Firefox
package: build
	$(WEB-EXT) build --source-dir ./build --overwrite-dest

# Launch firefox with the addon loaded and open the debug page and console
debug: build
	$(WEB-EXT) run --source-dir ./build --start-url "about:debugging#addons" --browser-console

clean:
	rm -fr ./build

build/browser-polyfill.js: node_modules/webextension-polyfill/dist/browser-polyfill.js
	./scripts/mkdir-cp.sh $? $@

build/background.js: src/background.js
	$(ROLLUP) --config rollup.conf.background.js

build/manifest.json: src/manifest.json
	./scripts/mkdir-cp.sh $? $@

build/cookie-sniper.css: src/cookie-sniper.css
	./scripts/mkdir-cp.sh $? $@

build/options.html: src/options.html
	./scripts/mkdir-cp.sh $? $@

build/options.js: src/options.js \
	src/EventTarget.js \
	src/CookieList.js \
	src/UrlRow.js \
	src/CookieRow.js
	$(ROLLUP) --config rollup.conf.options.js

build/icons/cookie-sniper-19.png: media/cookie-sniper.svg
	mkdir --parents build/icons
	inkscape --export-png $@ --export-width=19 --export-height=19 $?

build/icons/cookie-sniper-38.png: media/cookie-sniper.svg
	mkdir --parents build/icons
	inkscape --export-png $@ --export-width=38 --export-height=38 $?

build/icons/cookie-sniper-48.png: media/cookie-sniper.svg
	mkdir --parents build/icons
	inkscape --export-png $@ --export-width=48 --export-height=48 $?

build/icons/cookie-sniper-96.png: media/cookie-sniper.svg
	mkdir --parents build/icons
	inkscape --export-png $@ --export-width=96 --export-height=96 $?

# 128px icon is required for AMO
amo/cookie-sniper-128.png: media/cookie-sniper.svg
	mkdir --parents build/icons
	inkscape --export-png $@ --export-width=128 --export-height=128 $?

build/icons/delete.svg: media/delete.svg
	./scripts/mkdir-cp.sh $? $@

build/icons/new.svg: media/new.svg
	./scripts/mkdir-cp.sh $? $@

build/icons/save.svg: media/save.svg
	./scripts/mkdir-cp.sh $? $@

build/README.md: README.md
	./scripts/mkdir-cp.sh $? $@

build/LICENSE: LICENSE
	./scripts/mkdir-cp.sh $? $@
