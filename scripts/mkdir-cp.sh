#!/usr/bin/env bash

#echo "Copying $1 to $2"

src="$1"
dst="$2"
folder=`dirname "$dst"`
mkdir --parents "$folder"
cp "$src" "$dst"
