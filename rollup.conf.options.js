// rollup.config.js
import resolve from 'rollup-plugin-node-resolve';

export default {
  input: 'src/options.js',
  output: {
    file: 'build/options.js',
    format: 'iife'
  },
  plugins: [ resolve() ]
};
