// rollup.config.js
import resolve from 'rollup-plugin-node-resolve';

export default {
  input: 'src/background.js',
  output: {
    file: 'build/background.js',
    format: 'iife'
  },
  plugins: [ resolve() ]
};
