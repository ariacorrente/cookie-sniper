# Cookie Sniper

Browser addon that automatically removes specific cookies on indicated URLs.

## Description

Cookie Sniper is a tool to remove cookies automatically using a black-list. You
tell an URL where to search and the name of the cookie and this add-on will
remove the indicated cookie at every page reload.

The purpose of Cookie Sniper is to remove cookies from a website with a minimal
impact. It is not good as an anti-tracking tool for the whole internet but can
be helpful to fix errors or bad behaviors on specific websites.

**Permissions requested**

- *Access your data for all websites*: to interact with the cookies.
- *Access browser activity during navigation*: to detect on what page to run.

**Usage**

1. Choose the cookie name and on the URL where the add-on must run. This can be
    done with the "Storage Inspector" of the Firefox web development tools.
2. In the Cookie Sniper preferences page click  the "Add URL" button and insert
    the URL. The add-on will trigger for matches if the URL (without fragment
    identifier) starts with the specified string.
3. Click the "Add cookie rule" to add a new line inside the URL block just
    created. Paste the name of the cookie under the "Cookie name" column. The
    "Description" field is not used by the add-on but can be useful to the user
    to remember the purpose of the cookie.
4. Click the "Save" button to apply the changes.

## Build instructions

Supported browsers:

- Firefox
- Chrome / Chromium

Source code repository:

    https://gitlab.com/ariacorrente/cookie-sniper

The addon is developed with a GNU/Linux operating system and Firefox, the
following instructions are not tested with other systems.

Required tools and version used:

- npm (6.14.3): Will install rollup, web-ext and the javascript libraries
- nodejs (v12.16.1)
- make (4.1)
- inkscape (0.92.3): To convert SVG icons to PNG format

Commands to generate the compressed addon package starting from the git
repository:

    git clone https://gitlab.com/ariacorrente/cookie-sniper
    cd cookie-sniper
    npm install
    make package

Other useful make targets:

- `make package`: builds the source-dir of the addon
- `make debug`: launches the browser loading the addon for debugging

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
