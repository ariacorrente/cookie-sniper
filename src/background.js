import { getClog } from 'js-tools';

let clog = getClog("CookieSniper.background", true);
clog("Started loading");

let cookieList = [];

function getCookiesDescription(url) {
  let cookiesFound = [];
  let tempCookies = [];
  let cookie;

  for (let iUrl = 0; iUrl < cookieList.length; ++iUrl) {
    clog(`Comparing ${url} to ${cookieList[iUrl].url}`);

    if (url.startsWith(cookieList[iUrl].url)) {
      tempCookies = cookieList[iUrl].cookies;
      clog(`Found an url: ${cookieList[iUrl].url}`);

      for ( let iCookie = 0; iCookie < tempCookies.length; ++iCookie) {
        cookie = {
          cookieName: tempCookies[iCookie].cookieName
        };
        clog("Cookie to remove:", cookie);
        cookiesFound.push(cookie);
      }

    }

  }

  return cookiesFound;
}

function getCookie(tabs) {
  let getting = browser.cookies.get({
    url: "/",
    name: cookieName
  });
  getting.then(logCookies, onErrorGettingCookie);
}

function setCookie(tabs) {
  let setting = browser.cookies.set({
    url: "/",
    name: cookieName,
    value: "nuia"
  });
  setting.then(onSet, onErrorSettingCookie);
}

function removeCookie(tabs) {
  let cookies = getCookiesDescription(tabs.url);

  for (let i = 0; i < cookies.length; ++i) {
    if (cookies[i] != null) {
      let removing = browser.cookies.remove({
        url: tabs.url,
        name: cookies[i].cookieName
      });
      removing.then(onRemoved, onErrorRemovingCookie);
    }
  }
}

function handleOnDomContentLoaded(tabs) {
  clog(`Searching for cookies in the whitelisted URL: ${tabs.url}`);
  //getCookie(tabs);
  //setCookie(tabs);
  removeCookie(tabs);

  // Display the page-action icon
  browser.pageAction.setTitle({
    tabId: tabs.tabId,
    title: "Cookie Sniper ran on this page"
  });
  browser.pageAction.show(tabs.tabId);
}

function initWebNavigation() {
  let url = [],
    filter = {};

  //Remove old listener if it is installed
  if ( browser.webNavigation.onDOMContentLoaded.hasListener(handleOnDomContentLoaded) ) {
      browser.webNavigation.onDOMContentLoaded.removeListener(handleOnDomContentLoaded);
  }

  if (cookieList === undefined || cookieList.length === 0) {
    clog("Cookie list empty, not installing any listener for the webNavigation");
    return;
  }

  for(let i = 0; i < cookieList.length; i++) {
    url.push( {urlPrefix: cookieList[i].url} );
  }
  filter.url = url;
  browser.webNavigation.onDOMContentLoaded.addListener(
    handleOnDomContentLoaded,
    filter);
  clog("Installed filter:", filter);
}

function logCookies(cookies) {
  clog("logCookies: ", cookies.value);
}

function onRemoved(cookie) {
  if (cookie != null) {
    clog(`Cookie removed: ${cookie.name}`);
  } else {
    clog("Cookie to remove not found");
  }
}

function onSet(cookie) {
  clog(`Cookie set: ${cookie.name}`);
}

function onErrorRemovingCookie(error) {
  clog(`Error removing cookie: ${error.name}`);
}

function onErrorGettingCookie(error) {
  clog(`Error getting cookie: ${error.name}`);
}

function onErrorSettingCookie(error) {
  clog(`Error setting cookie: ${error.name}`);
}

function restoreOptions() {

  function applySettings(result) {
    if (result.cookieList !== undefined) {
      cookieList = result.cookieList;
    } else {
      cookieList = [];
    }
    initWebNavigation();
  }

  function onErrorReadingStorage(error) {
    clog(`Error restoring options: ${error}`);
  }

  var getting = browser.storage.sync.get(["cookieList"]);
  getting.then(applySettings, onErrorReadingStorage);
}

/*
 * 1. load from storage the cookies to be removed
 * 2. init WebNavigation to listen for URLs in whitelist
 * 3. if page is detected then run the promise to delete cookie
 * 4. wait for promise fulfillment
 */
browser.storage.onChanged.addListener(restoreOptions);
restoreOptions();

// Open settings page when clicking the page-action icon
browser.pageAction.onClicked.addListener(
  ()=>{ browser.runtime.openOptionsPage(); }
);

clog("Load completed");
