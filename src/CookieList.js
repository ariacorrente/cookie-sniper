import UrlRow from "./UrlRow.js";
import EventTarget from './EventTarget.js';
import { getClog, buildDom } from 'js-tools';


class CookieList extends EventTarget {

constructor() {
  super();
  this.clog = getClog("CookieSniper.CookieList", true);
  this.rows = [];
  this.render();
}

_removeAllChilds(targetNode) {
  while (targetNode.firstChild) {
      targetNode.removeChild(targetNode.firstChild);
  }
}

setCookies(cookies) {
  let row;

  this._removeAllChilds(this.rowsWrapper);
  this.rows = [];
  if (cookies === undefined) return;

  for (let i = 0; i < cookies.length; i++) {
    row = new UrlRow;
    row.on("remove", this._onRemoveRow.bind(this));
    row.setRow(cookies[i]);
    this.rows[i] = row;
    this.rowsWrapper.appendChild(row.getElement());
  }
}

getCookies() {
  let cookies = [];

  for (let i = 0; i < this.rows.length; i++) {
    cookies[i] = this.rows[i].getRow();
  }
  return cookies;
}

getElement() {
  return this.element;
}

_onRemoveRow(event) {
  this.clog("Must remove row", event.data.getRow().url);
  this.rowsWrapper.removeChild(event.data.getElement());
  let index = this.rows.indexOf(event.data)
  if (index != -1) {
    // remove from the array starting from "index" for 1 element
    this.rows.splice(index, 1);
  }
}

_onButtonAddClicked(event) {
  event.preventDefault();

  let row = new UrlRow;
  row.on("remove", this._onRemoveRow.bind(this));
  this.rows.push(row);
  this.rowsWrapper.appendChild(row.getElement());
  this.clog("_onButtonAddClicked", event);
}

render() {
  if (this.element === undefined) {
    const data = {
      tag: "div",
      class: "cookie-table",
      children: [
        {
          tag: "div",
          class: "urls-wrapper"
        },
        {
          tag: "div",
          class: "table-footer",
          children: {
            tag: "button",
            class: "button secondary",
            eventClick: this._onButtonAddClicked.bind(this),
            children: [
              {
                tag: "img",
                src: "icons/new.svg"
              },
              {
                text: "Add URL"
              }
            ]
          }
        }
      ]
    };
    this.element = buildDom( data );
    this.rowsWrapper = this.element.querySelector(".urls-wrapper");
  }
}

}

export default CookieList;
