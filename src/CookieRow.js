import EventTarget from './EventTarget.js';
import { getClog, buildDom } from 'js-tools';

/*
 * Structure of the model passed through setCookie and getCookie;
 * {
 *  cookieName: "string: the name of the cookie",
 *  description: "string: optional info about the purpose of this cookie"
 * }
 */
class CookieRow extends EventTarget {

constructor() {
  super();
  this.clog = getClog("CookieSniper.CookieRow", true);
  this.render();
}

setCookie(cookie) {
  this.elementDescription.value = cookie.description;
  this.elementCookieName.value = cookie.cookieName;
}

getCookie() {
  return {
    cookieName: this.elementCookieName.value,
    description: this.elementDescription.value
  };
}

getElement() {
  return this.element;
}

_onButtonRemoveClicked( event ) {
  event.preventDefault();
  this.clog("ButtonRemove for url clicked");
  this.emit( { type: "remove", data: this } );
}

render() {
  if (this.element === undefined) {
    const data = {
      tag: "div",
      class: "cookie-row",
      children: [
        {
          tag: "input",
          type: "text",
          class: "cookie-name"
        },
        {
          tag: "input",
          type: "text",
          class: "cookie-description"
        },
        {
          tag: "button",
          class: "remove-cookie button secondary icon-only",
          eventClick: this._onButtonRemoveClicked.bind(this),
          title: "Remove cookie rule",
          children: {
            tag: "img",
            src: "icons/delete.svg"
          }
        }
      ]
    };
    this.element = buildDom( data );
    this.elementDescription = this.element.querySelector(".cookie-description");
    this.elementCookieName = this.element.querySelector(".cookie-name");
  }
}

}

export default CookieRow;
