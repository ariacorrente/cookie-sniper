/*
  For handling events between the objects is better NOT to use the
  event handler used by the DOM, more info here:

    http://stackoverflow.com/a/37456506

  Source code taken from:

    https://developer.mozilla.org/en-US/docs/Web/API/EventTarget

  And changed a bit.

  Structure of an event event:

    {
      type: "event-name",
      data: "event-data"
    }

  Remember to bind the correct "this" when connecting the event listener:

    emitterObject.on( "eventName", this.onEventName.bind( this );
*/

class EventTarget {

constructor() {
    this.listeners = {};
}

on( type, callback ) {
  if( !( type in this.listeners ) ) {
    this.listeners[ type ] = [];
  }
  this.listeners[ type ].push( callback );
}

off( type, callback ) {
  if( !( type in this.listeners ) ) {
    return;
  }
  var stack = this.listeners[ type ];
  for ( var i = 0, l = stack.length; i < l; i++ ){
    if( stack[ i ] === callback ){
      stack.splice( i, 1 );
      return this.off( type, callback );
    }
  }
}

emit( event ) {
  if( !( event.type in this.listeners ) ) {
    return;
  }
  var stack = this.listeners[ event.type ];
  event.target = this;
  for( var i = 0, l = stack.length; i < l; i++ ) {
    stack[i].call( this, event );
  }
}

};

export default EventTarget;
