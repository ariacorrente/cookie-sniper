import { getClog, buildDom } from "js-tools";
import CookieList from "./CookieList.js";

let clog = getClog("CookieSniper.options", true);
clog("Loading options");

let cookieList;
let dialogReset;

function saveOptions(e) {
  e.preventDefault();

  browser.storage.sync.set({
    cookieList: cookieList.getCookies()
  });
}

function resetOptions() {

  function onCleared() {
    clog("Storage cleared successfully");
    buildPage();
  }

  function onError(event) {
    clog("Error clearing storage", event);
  }

  var clearStorage = browser.storage.sync.clear();
  clearStorage.then(onCleared, onError);
}

function restoreOptions() {

  function setCurrentChoice(result) {
    cookieList.setCookies(result.cookieList);
  }

  function onError(error) {
    clog(`Error restoring options: ${error}`);
  }

  var getting = browser.storage.sync.get(["cookieList"]);
  getting.then(setCurrentChoice, onError);
}

function onRemoveElement(event) {
  clog("onRemoveElement", event);
}

function buildPage() {
  document.querySelector("form").addEventListener("submit", saveOptions);
  document.querySelector(".button-reset").addEventListener("click", ()=>{
    setDialogResetVisibility(true);
  });
  document.querySelector(".button-reset-yes").addEventListener("click", ()=>{
    resetOptions();
    setDialogResetVisibility(false);
  });
  document.querySelector(".button-reset-no").addEventListener("click", ()=>{
    setDialogResetVisibility(false);
  });
  dialogReset = document.querySelector(".dialog-reset");
  let placeholder = document.querySelector(".cookie-table");
  cookieList = new CookieList();
  placeholder.parentNode.replaceChild(cookieList.getElement(), placeholder);
  restoreOptions();
}

function setDialogResetVisibility(mustBeVisible) {
  if (mustBeVisible) {
    dialogReset.classList.add("visible");
  } else {
    dialogReset.classList.remove("visible");
  }
}

document.addEventListener("DOMContentLoaded", buildPage);
