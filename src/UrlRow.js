import CookieRow from './CookieRow.js';
import EventTarget from './EventTarget.js';
import { getClog, buildDom } from 'js-tools';

/*
 * Structure of the model passed thrugh setUrl and getUrl:
 * {
 *  url: "string: URL where the addon must run",
 *  cookies: "object: see CookieRow for the structure"
 * }
 */
class UrlRow extends EventTarget {

constructor() {
  super();
  this.clog = getClog("CookieSniper.UrlRow", true);
  this.rows = [];
  this.render();
}

_removeAllChilds(targetNode) {
  while (targetNode.firstChild) {
      targetNode.removeChild(targetNode.firstChild);
  }
}

setRow(row) {
  this.elementUrl.value = row.url;
  this._setCookies(row.cookies);
}

getRow() {
  return {
    url: this.elementUrl.value,
    cookies: this._getCookies()
  };
}

getElement() {
  return this.element;
}

_setCookies(cookies) {
  let row;

  this._removeAllChilds(this.rowsWrapper);
  this.rows = [];
  if (cookies === undefined) return;

  for (let i = 0; i < cookies.length; i++) {
    row = new CookieRow;
    row.on("remove", this._onButtonRemoveCookie.bind(this));
    row.setCookie(cookies[i]);
    this.rows[i] = row;
    this.rowsWrapper.appendChild(row.getElement());
  }
}

_getCookies() {
  let cookies = [];
  let cookie;
  for (let i = 0; i < this.rows.length; i++) {
    cookie = this.rows[i].getCookie();
    cookies[i] = cookie;
  }
  return cookies;
}

_onButtonRemoveUrlClicked( event ) {
  event.preventDefault();
  this.clog("ButtonRemove for url clicked");
  this.emit( { type: "remove", data: this } );
}

_onButtonRemoveCookie(event) {
  this.clog("Must remove row", event.data.getCookie().cookieName);
  this.rowsWrapper.removeChild(event.data.getElement());
  let index = this.rows.indexOf(event.data)
  if (index != -1) {
    // remove from the array starting from "index" for 1 element
    this.rows.splice(index, 1);
  }
};

_onButtonAddCookieClicked(event) {
  event.preventDefault();
  this.clog("ButtonAddCookie clicked", event);
  let row = new CookieRow;
  row.on("remove", this._onButtonRemoveCookie.bind(this));
  this.rows.push(row);
  this.rowsWrapper.appendChild(row.getElement());
}

render() {
  if (this.element === undefined) {
    const data = {
      tag: "div",
      class: "url-row",
      children: [
        {
          tag: "label",
          class: "label-url",
          text: "URL:",
          children: {
            tag: "input",
            type: "text",
            class: "url"
          }
        },
        {
          tag: "button",
          class: "remove-url button secondary icon-only",
          eventClick: this._onButtonRemoveUrlClicked.bind(this),
          title: "Remove URL",
          children: {
            tag: "img",
            src: "icons/delete.svg"
          }
        },
        {
          tag: "div",
          class: "table-header",
          children: [
            {
              tag: "div",
              text: "Cookie name"
            },
            {
              tag: "div",
              text: "Description"
            },
            {
              tag: "div",
              text: ""
            }
          ]
        },
        {
          tag: "div",
          class: "cookies-wrapper"
        },
        {
          tag: "div",
          class: "table-footer",
          children: {
            tag: "button",
            class: "addCookie button secondary",
            eventClick: this._onButtonAddCookieClicked.bind(this),
            children: [
              {
                tag: "img",
                src: "icons/new.svg"
              },
              {
                text: "Add cookie rule"
              }
            ]
          }
        }
      ]
    };
    this.element = buildDom( data );
    this.elementUrl = this.element.querySelector(".url");
    this.rowsWrapper = this.element.querySelector(".cookies-wrapper");
  }
}

}

export default UrlRow;
